SCREEN = 1;
EDGE = 1;
BACK = 1;
    
$fa = 3;
$fs = 0.5;

BOARDWIDTH = 50;
BOARDHEIGHT = 69;

THICKNESS = 6;
LIP = 7.5;
SCREENTHICKNESS = 4;

module holes( r ) {
    
    translate( [ -THICKNESS / 2, -THICKNESS / 2, -100 ] )
	cylinder( r = r, h = 200 );
    
    translate( [ -THICKNESS / 2, BOARDHEIGHT + THICKNESS / 2, -100 ] )
	cylinder( r = r, h = 200 );
    
    translate( [ BOARDWIDTH + THICKNESS / 2, -THICKNESS / 2, -100 ] )
	cylinder( r = r, h = 200 );
    
    translate( [ BOARDWIDTH + THICKNESS / 2, BOARDHEIGHT + THICKNESS / 2, -100 ] )
	cylinder( r = r, h = 200 );
}

// Screen
if( SCREEN ) {
    translate( [ -THICKNESS, -THICKNESS, 0 ] )
	cube( size = [ BOARDWIDTH + THICKNESS * 2, LIP, 2 ] );
    translate( [ -THICKNESS, -THICKNESS, 0 ] )
	cube( size = [ LIP, BOARDHEIGHT + THICKNESS * 2, 2 ] );
    translate( [ -THICKNESS, BOARDHEIGHT + THICKNESS - LIP, 0 ] )
	cube( size = [ BOARDWIDTH + THICKNESS * 2, LIP, 2 ] );
    translate( [ BOARDWIDTH + THICKNESS - LIP, -THICKNESS, 0 ] )
	cube( size = [ LIP, BOARDHEIGHT + THICKNESS * 2, 2 ] );

    difference() {
	union() {
	    translate( [ -THICKNESS, -THICKNESS, -SCREENTHICKNESS ] )
		cube( size = [ BOARDWIDTH + THICKNESS * 2, THICKNESS - 0.5, SCREENTHICKNESS ] );
	    translate( [ -THICKNESS, -THICKNESS, -SCREENTHICKNESS ] )
		cube( size = [ THICKNESS - 0.5, BOARDHEIGHT + THICKNESS * 2, SCREENTHICKNESS ] );
	    translate( [ -THICKNESS, BOARDHEIGHT + 0.5, -SCREENTHICKNESS ] )
		cube( size = [ BOARDWIDTH + THICKNESS * 2, THICKNESS - 0.5, SCREENTHICKNESS ] );
	    translate( [ BOARDWIDTH + 0.5, -THICKNESS, -SCREENTHICKNESS ] )
		cube( size = [ THICKNESS - 0.5, BOARDHEIGHT + THICKNESS * 2, SCREENTHICKNESS ] );
	}

	holes( 1 );
    }
}

// Edge
if( EDGE ) {
    difference() {
	union() {
	    translate( [ -1, 20, -9 ] )
		cube( size = [ 3, 30, 2 ] );

	    translate( [ BOARDWIDTH - 2, 20, -9 ] )
		cube( size = [ 3, 30, 2 ] );
	
	    translate( [ -THICKNESS, -THICKNESS, -21.6 ] )
		cube( size = [ BOARDWIDTH + THICKNESS * 2, THICKNESS - 0.2, 14.6 ] );

	    translate( [ -THICKNESS, -THICKNESS, -21.6 ] )
		cube( size = [ THICKNESS - 0.2, BOARDHEIGHT + THICKNESS * 2, 14.6 ] );

	    translate( [ -THICKNESS, BOARDHEIGHT + 0.2, -21.6 ] )
		cube( size = [ BOARDWIDTH + THICKNESS * 2, THICKNESS - 0.2, 14.6 ] );
	
	    translate( [ BOARDWIDTH + 0.2, -THICKNESS, -21.6 ] )
		cube( size = [ THICKNESS - 0.2, BOARDHEIGHT + THICKNESS * 2, 14.6 ] );
	}

	holes( 1.5 );

	// USB
	translate( [ 34, -10, -100 ] )
	    cube( size = [ 13, 30, 89.4 ] );

	// Antenna
	translate( [ -10, 9, -14 ] )
	    cube( size = [ 20, 11, 8 ] );
    }
}

// Back
if( BACK ) {
    difference() {
	translate( [ -THICKNESS, -THICKNESS, -30 ] )
	    cube( size = [ BOARDWIDTH + THICKNESS * 2, BOARDHEIGHT + THICKNESS * 2, 3 ] );

	holes( 1.5 );
    }
}
